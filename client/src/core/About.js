import React from "react";
import Menu from "./Menu";



const About = () => {
    return (
        <div className="about">
            <Menu/>
            <h1 style={{textAlign:"center",fontFamily:"Stencil Regular"}}>
                Developed By
            </h1>
           <div className="aboutUS" style={{display:"inline", marginLeft:"500px"}}>
            <div className = "card h-50" style={{width:"200px"}}>
                    <div className = "card-header bg-dark text-white">Garvit Rajput - 1900820100046</div>
                </div>
                <div className = "card h-50" style={{width:"200px"}}>
                    <div className = "card-header bg-dark text-white">Divyanshu Gahlot - 1900820100042</div>
                </div>
                <div className = "card h-50" style={{width:"200px"}}>
                    <div className = "card-header bg-dark text-white">Bhupendra Singh Gola - 1900820100032</div>
                </div>
                <div className = "card h-50" style={{width:"200px"}}>
                    <div className = "card-header bg-dark text-white">Harsh Chaudhary - 1900820100049</div>
                </div>
                <div className = "card h-50" style={{width:"200px"}}>
                    <div className = "card-header bg-dark text-white">Abhishek Rana - 1900820100008</div>
                </div>
           </div>
         </div>
    )
}

export default About
